#include <iostream>
#include "Website.h"

std::map<std::string, std::string> readManifesto(std::string manifestoPath);
std::string readVersion(std::string versionPath);
std::vector<Website> readSupportedWebsites(std::string supportedWebsitesPath);

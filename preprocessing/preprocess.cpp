#include "readFunctions.h"

int main(void) {
  std::map<std::string, std::string> manifesto = readManifesto("./MANIFESTO.md");
  std::string version =  readVersion("./data/version");
  std::vector<Website> websites = readSupportedWebsites("./data/supportedWebsites.csv");

  // Create background and content scripts elements and url list
  std::string backgroundScripts;
  std::stringstream elements;
  std::stringstream urlList;
  for (auto website: websites) {
    backgroundScripts += website.printBackgroundScripts();
    elements << website.printElement();
    urlList << website.printUrls();
    if (website.getName() != websites.back().getName()) {
      elements << "," << std::endl;
      urlList << ", ";
    }
  }
  
  // Create manifest.json
  std::ofstream manifestFile;
  manifestFile.open("./manifest.json");
  
  std::ifstream manifestTemplate("./data/manifestTemplate.txt");
  std::string manifestLine;
  while (std::getline(manifestTemplate, manifestLine)) {
    replace(manifestLine, "<version>", version);
    replace(manifestLine, "<background_scripts>", backgroundScripts.substr(0, backgroundScripts.length() - 2));
    replace(manifestLine, "<content_scripts>", elements.str());
    replace(manifestLine, "<permissions_urls>", urlList.str());
    manifestFile << manifestLine << std::endl;
  }
  manifestFile.close();

  
  // Create CHANGES.md
  std::ofstream changesFile;
  changesFile.open("./CHANGES.md");
  
  for (auto website: websites) {
    std::string websiteName = website.getName();
    changesFile << "# " << capitalize(websiteName) << std::endl;
    changesFile << website.printComments(manifesto) << std::endl;
  }
  
  changesFile.close();
  
  return 0;
}

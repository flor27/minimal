![minimal logo](./pictures/logo/minimalAvatar512.png)

# Minimal, the browser extension for peace of mind
Minimal is a browser extension to experience a minimal, less attention grabbing internet experience. Internet should be a tool, not a trap.

Minimal is driven by core values:

- The user must actively make choices by themselves.
- A user should easily find the content they are searching for on a page, not the content a platform wants them to see.
- A service provider can convince the user to use their platform, but only through their services' inner quality.

[![Get the add-on](https://addons.cdn.mozilla.net/static/img/addons-buttons/AMO-button_1.png)](https://addons.mozilla.org/firefox/addon/minimal-internet-experience/)

To get a complete list of the changes made to the user experience by the minimal browser extension, check the [CHANGES file](./CHANGES.md). Those changes are following [minimal's manifesto](MANIFESTO.md).

This extension is being developed for the greater good, it is an open source project, you can check its code and contribute.

# How to contribute

## As a user
If you spot something that you think minimal should act upon on a common website let us know by creating a ticket [here](https://gitlab.com/timkrief/minimal/issues).

If you encounter any problem while using minimal, please create a new [issue](https://gitlab.com/timkrief/minimal/issues).

## As a (web)designer
If you have ideas about what a common website should look like or act like for it to be minimal, please, add your ideas [here](https://gitlab.com/timkrief/minimal/issues). 
Please, try to follow [minimal's manifesto](./MANIFESTO.md).

## As a (web)developer
This is a browser extension, if you are not familiar about how it works, [here is a guide](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions).

When contributing, if you make a new change, add a comment to explain it.
Do not edit CHANGES.md manually, the comment will be added to changes automatically.

The comment has to fit on one line and has to follow this pattern:
1. `/* - ` 
2. The explanation of the change
3. ` - ` 
4. A code of a rule from the [manifesto](./MANIFESTO.md)
5. ` */`

Changes must follow at least one rule from the [manifesto](./MANIFESTO.md). Rules codes are the first letter of a main section followed by the number of the rule. Multiple codes can be specifed, separated by spaces.

Exemples of the comment pattern:

```
/* - Explaination of the change - C3 P1 */
```
```
/* - Explaination - U2 */
```

To test the extension and to update the CHANGES.md file once changes were made, use "make" in a terminal.
It will generate necessary files and zip them. Then you will be able to use "minimal.zip".
